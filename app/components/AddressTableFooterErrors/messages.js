/*
 * AddressTableFooterErrors Messages
 *
 * This contains all the text for the AddressTableFooterErrors component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.AddressTableFooterErrors.header',
    defaultMessage: 'This is the AddressTableFooterErrors component !',
  },
  check_balances_error: {
    id: 'app.components.AddressTableFooterErrors.check_balances_error',
    defaultMessage: 'Ошибка проверки баланса',
  },
  add_addresss_error: {
    id: 'app.components.AddressTableFooterErrors.add_addresss_error',
    defaultMessage: 'Ошибка добавления адреса',
  },
  update_exchange_rates_error: {
    id: 'app.components.AddressTableFooterErrors.update_exchange_rates_error',
    defaultMessage: 'Ошибка обновления тарифов',
  },
});
