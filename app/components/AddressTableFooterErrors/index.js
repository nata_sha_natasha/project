/**
*
* AddressTableFooterErrors
*
*/

import React from 'react';
import styled from 'styled-components';
import { Alert } from 'antd';
import PropTypes from 'prop-types';

const Div = styled.div`
  max-width: 490px;  
  margin: auto;
  margin-top: 35px;
`;
const PaddedAlert = styled(Alert)`
  margin-top: 15px;
`;
// import { FormattedMessage } from 'react-intl';
import messages from './messages';

function AddressTableFooterErrors(props) {
  const { checkingBalancesError, addressListError, getExchangeRatesError } = props;
  return (
    <Div>
      {checkingBalancesError ? <Alert type="error" message={messages.check_balances_error.defaultMessage} description={checkingBalancesError} /> : null}
      {addressListError ? <PaddedAlert type="error" message={messages.add_addresss_error.defaultMessage} description={addressListError} /> : null}
      {getExchangeRatesError ? <PaddedAlert type="error" message={messages.update_exchange_rates_error.defaultMessage} description={getExchangeRatesError.toString()} /> : null}
    </Div>
  );
}

AddressTableFooterErrors.propTypes = {
  checkingBalancesError: PropTypes.oneOfType([PropTypes.object, PropTypes.string, PropTypes.bool]),
  addressListError: PropTypes.oneOfType([PropTypes.object, PropTypes.string, PropTypes.bool]),
  getExchangeRatesError: PropTypes.oneOfType([PropTypes.object, PropTypes.string, PropTypes.bool]),
};

export default AddressTableFooterErrors;
