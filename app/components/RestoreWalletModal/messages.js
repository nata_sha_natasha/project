/*
 * RestoreWalletModal Messages
 *
 * This contains all the text for the RestoreWalletModal component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RestoreWalletModal.header',
    defaultMessage: 'This is the RestoreWalletModal component !',
  },
  restore_wallet: {
    id: 'app.components.RestoreWalletModal.restore_wallet',
    defaultMessage: 'Восстановить кошелек',
  },
  restore: {
    id: 'app.components.RestoreWalletModal.restore',
    defaultMessage: 'Восстановить',
  },
  address_generation: {
    id: 'app.components.RestoreWalletModal.address_generation',
    defaultMessage: 'используется для генерации адресов',
  },
  enter_seed: {
    id: 'app.components.RestoreWalletModal.enter_seed',
    defaultMessage: 'Введите источник',
  },
  password_for_keystore_encryption: {
    id: 'app.components.RestoreWalletModal.header',
    defaultMessage: 'Введите пароль для шифрования хранилища ключей',
  },
});
