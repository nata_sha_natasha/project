/*
 * PageFooter Messages
 *
 * This contains all the text for the PageFooter component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.PageFooter.header',
    defaultMessage: 'This is the PageFooter component !',
  },
  open_source_wallet: {
    id: 'app.components.PageFooter.open_source_wallet',
    defaultMessage: 'Ethereum и ERC20 с открытым исходным кодом (Github)',
  },
  created_using: {
    id: 'app.components.PageFooter.created_using',
    defaultMessage: 'Создано с использованием: eth-lightwallet, React.js, Ant design ...',
  },
  donate_for_development: {
    id: 'app.components.PageFooter.donate_for_development',
    defaultMessage: 'Пожертвовать для развития: (ETH / ERC20)',
  },
});
