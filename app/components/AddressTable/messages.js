/*
 * AddressTable Messages
 *
 * This contains all the text for the AddressTable component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.AddressTable.header',
    defaultMessage: 'This is the AddressTable component !',
  },
  icon: {
    id: 'app.components.AddressTable.icon',
    defaultMessage: 'Иконка',
  },
  address: {
    id: 'app.components.AddressTable.address',
    defaultMessage: 'Адрес',
  },
  token: {
    id: 'app.components.AddressTable.token',
    defaultMessage: 'Токен',
  },
  balance: {
    id: 'app.components.AddressTable.balance',
    defaultMessage: 'Баланс',
  },
  action: {
    id: 'app.components.AddressTable.action',
    defaultMessage: 'Действие',
  },
  no_data: {
    id: 'app.components.AddressTable.no_data',
    defaultMessage: 'Нет данных',
  },
});
