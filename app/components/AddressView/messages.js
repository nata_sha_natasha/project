/*
 * AddressView Messages
 *
 * This contains all the text for the AddressView component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.AddressView.header',
    defaultMessage: 'Address list: ',
  },
  generate_keystore_error: {
    id: 'app.components.AddressView.generate_keystore_error',
    defaultMessage: 'Генерировать ошибку в хранилище ключей',
  },
  loading: {
    id: 'app.components.AddressView.loading',
    defaultMessage: 'Загрузка...',
  },
});
