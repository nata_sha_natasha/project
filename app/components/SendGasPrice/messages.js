/*
 * SendGasPrice Messages
 *
 * This contains all the text for the SendGasPrice component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.SendGasPrice.header',
    defaultMessage: 'Gas price: ',
  },
  gas_price: {
    id: 'app.components.SendGasPrice.gas_price',
    defaultMessage: 'Цена на газ (Gwei):',
  },
});
