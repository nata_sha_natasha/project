/*
 * IconButton Messages
 *
 * This contains all the text for the IconButton component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.IconButton.header',
    defaultMessage: 'This is the IconButton component !',
  },
  confirm: {
    id: 'app.components.IconButton.confirm',
    defaultMessage: 'Подтвердить',
  },
  confirm: {
    id: 'app.components.IconButton.confirm',
    defaultMessage: 'Подтвердить',
  },
  abort: {
    id: 'app.components.IconButton.abort',
    defaultMessage: 'Отменить',
  },
  click_to_retry: {
    id: 'app.components.IconButton.click_to_retry',
    defaultMessage: 'Нажмите, чтобы повторить попытку',
  },
});
