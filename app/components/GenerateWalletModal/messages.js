/*
 * GenerateWalletModal Messages
 *
 * This contains all the text for the GenerateWalletModal component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.GenerateWalletModal.header',
    defaultMessage: 'This is the GenerateWalletModal component !',
  },
  new_wallet: {
    id: 'app.components.GenerateWalletModal.new_wallet',
    defaultMessage: 'Новый кошелек',
  },
  create: {
    id: 'app.components.GenerateWalletModal.сreate',
    defaultMessage: 'Создать',
  },
  the_seed_is_imposible_to_recover_if_lost: {
    id: 'app.components.GenerateWalletModal.the_seed_is_imposible_to_recover_if_lost',
    defaultMessage: 'Источник нельзя восстановить при потере',
  },
  copy_the_generated_seed_to_safe_location: {
    id: 'app.components.GenerateWalletModal.copy_the_generated_seed_to_safe_location',
    defaultMessage: 'Скопируйте сгенерированный источник в безопасное место.',
  },
  recover_lost_password_using_the_seed: {
    id: 'app.components.GenerateWalletModal.recover_lost_password_using_the_seed',
    defaultMessage: 'Восстановление потерянного пароля с использованием источника.',
  },
  seed: {
    id: 'app.components.GenerateWalletModal.seed',
    defaultMessage: 'Источник:',
  },
  password_for_browser_encryption: {
    id: 'app.components.GenerateWalletModal.password_for_browser_encryption',
    defaultMessage: 'Пароль для шифрования браузера:',
  },
});
