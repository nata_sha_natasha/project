/**
*
* GenerateWalletModal
*
*/

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
import { Modal, Button, Alert } from 'antd';

import messages from './messages';

function GenerateWalletModal(props) {
  const {
    isShowGenerateWallet,
    generateWalletLoading,
    // generateWalletError,
    seed,
    password,

    onGenerateWallet,
    onGenerateWalletCancel,
    onGenerateKeystore,
    } = props;

  return (
    <Modal
      visible={isShowGenerateWallet}
      title={messages.new_wallet.defaultMessage}
      onOk={onGenerateKeystore}
      onCancel={onGenerateWalletCancel}
      footer={[
        <Button key="submit" type="primary" size="large" onClick={onGenerateKeystore}>
          {messages.create.defaultMessage}
        </Button>,
      ]}
    >
      <Alert
        message={<b>{messages.the_seed_is_imposible_to_recover_if_lost.defaultMessage}</b>}
        description={<b>{messages.copy_the_generated_seed_to_safe_location.defaultMessage}<br />
                        HDPathString: m/44'/60'/0'/0.<br /> {messages.recover_lost_password_using_the_seed.defaultMessage}</b>} // eslint-disable-line
        type="warning"
        showIcon
        closable
      />
      <br />
      <Alert
        message={messages.seed.defaultMessage}
        description={<b>{seed}</b>}
        type="info"
      />
      <br />
      <Alert
        message={messages.password_for_browser_encryption.defaultMessage}
        description={<b>{password}</b>}
        type="info"
      />
      <br />
      <Button shape="circle" icon="reload" loading={generateWalletLoading} key="back" size="large" onClick={onGenerateWallet} />
    </Modal>
  );
}

GenerateWalletModal.propTypes = {
  isShowGenerateWallet: PropTypes.bool,
  generateWalletLoading: PropTypes.bool,
  /* generateWalletError: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.string,
    PropTypes.bool,
  ]), */
  seed: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  password: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  onGenerateWallet: PropTypes.func,
  onGenerateWalletCancel: PropTypes.func,
  onGenerateKeystore: PropTypes.func,
};

export default GenerateWalletModal;
