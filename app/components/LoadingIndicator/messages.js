/*
 * LoadingIndicator Messages
 *
 * This contains all the text for the LoadingIndicator component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.LoadingIndicator.header',
    defaultMessage: 'This is the LoadingIndicator component !',
  },
  name: {
    id: 'app.components.LoadingIndicator.name',
    defaultMessage: 'Название',
  },
});
