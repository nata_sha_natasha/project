/*
 * RestoreWallet Messages
 *
 * This contains all the text for the RestoreWallet component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RestoreWallet.header',
    defaultMessage: 'Enter seed to restore wallet',
  },
  enter_seed: {
    id: 'app.components.RestoreWallet.enter_seed',
    defaultMessage: 'Введите источник',
  },
  restore_from_seed: {
    id: 'app.components.RestoreWallet.restore_from_seed',
    defaultMessage: 'Восстановление из источника',
  },
});
