/*
 * AddressTableFooter Messages
 *
 * This contains all the text for the AddressTableFooter component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.AddressTableFooter.header',
    defaultMessage: 'This is the AddressTableFooter component !',
  },
  add_address: {
    id: 'app.components.AddressTableFooter.add_address',
    defaultMessage: 'Добавить адрес',
  },
  check_balances: {
    id: 'app.components.AddressTableFooter.check_balances',
    defaultMessage: 'Проверить баланс',
  },
  update_rates: {
    id: 'app.components.AddressTableFooter.update_rates',
    defaultMessage: 'Обновить тарифы',
  },
  select_tokens: {
    id: 'app.components.AddressTableFooter.select_tokens',
    defaultMessage: 'Выберите токены',
  },
});
