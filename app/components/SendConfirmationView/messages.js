/*
 * SendConfirmationView Messages
 *
 * This contains all the text for the SendConfirmationView component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.SendConfirmationView.header',
    defaultMessage: 'This is the SendConfirmationView component !',
  },
  checking_transaction: {
    id: 'app.components.SendConfirmationView.checking_transaction',
    defaultMessage: 'проверка транзакции...',
  },
  transaction_not_created: {
    id: 'app.components.SendConfirmationView.transaction_not_created',
    defaultMessage: 'Транзакция не создана',
  },
  transaction_is_valid: {
    id: 'app.components.SendConfirmationView.transaction_is_valid',
    defaultMessage: 'Транзакция действительна',
  },
  try_again: {
    id: 'app.components.SendConfirmationView.try_again',
    defaultMessage: 'Попробуйте снова',
  },
  send_ETH: {
    id: 'app.components.SendConfirmationView.send_ETH',
    defaultMessage: 'Отправить ETH',
  },
  back: {
    id: 'app.components.SendConfirmationView.back',
    defaultMessage: 'Назад',
  },
});
