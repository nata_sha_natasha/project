/*
 * SendFrom Messages
 *
 * This contains all the text for the SendFrom component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.SendFrom.header',
    defaultMessage: 'This is the SendFrom component !',
  },
  source: {
    id: 'app.components.SendFrom.source',
    defaultMessage: 'Источник:',
  },
});
