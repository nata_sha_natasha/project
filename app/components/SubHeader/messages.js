/*
 * SubHeader Messages
 *
 * This contains all the text for the SubHeader component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.SubHeader.header',
    defaultMessage: 'This is the SubHeader component !',
  },
  new: {
    id: 'app.components.SubHeader.new',
    defaultMessage: 'Новый кошелек',
  },
  restore: {
    id: 'app.components.SubHeader.restore',
    defaultMessage: 'Восстановить кошелек',
  },
  close: {
    id: 'app.components.SubHeader.close',
    defaultMessage: 'Закрыть кошелек',
  },
  deleted: {
    id: 'app.components.SubHeader.deleted',
    defaultMessage: 'Кошелек будет удален из памяти и локального хранилища',
  },
  confirm: {
    id: 'app.components.SubHeader.confirm',
    defaultMessage: 'Подтвердить',
  },
  abort: {
    id: 'app.components.SubHeader.abort',
    defaultMessage: 'Отменить',
  },
});
