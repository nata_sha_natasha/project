/*
 * AddressItem Messages
 *
 * This contains all the text for the AddressItem component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.AddressItem.header',
    defaultMessage: 'This is the AddressItem component !',
  },
  send: {
    id: 'app.components.AddressItem.send',
    defaultMessage: 'Отправить',
  },
  balance: {
    id: 'app.components.AddressItem.balance',
    defaultMessage: 'Баланс',
  },
});
