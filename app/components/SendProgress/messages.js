/*
 * SendProgress Messages
 *
 * This contains all the text for the SendProgress component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.SendProgress.header',
    defaultMessage: 'This is the SendProgress component !',
  },
  sending: {
    id: 'app.components.SendProgress.sending',
    defaultMessage: 'Отправка...',
  },
  send_error: {
    id: 'app.components.SendProgress.send_error',
    defaultMessage: 'Ошибка отправки',
  },
  send_sucessfull: {
    id: 'app.components.SendProgress.send_sucessfull',
    defaultMessage: 'Успешная отправка',
  },
});
