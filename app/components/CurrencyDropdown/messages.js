/*
 * CurrencyDropdown Messages
 *
 * This contains all the text for the CurrencyDropdown component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.CurrencyDropdown.header',
    defaultMessage: 'Currency Dropdown',
  },
  convert: {
    id: 'app.components.CurrencyDropdown.convert',
    defaultMessage: 'Конвертировать',
  },
  none: {
    id: 'app.components.CurrencyDropdown.none',
    defaultMessage: 'Никто',
  },
});
