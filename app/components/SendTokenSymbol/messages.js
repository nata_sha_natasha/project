/*
 * SendTokenSymbol Messages
 *
 * This contains all the text for the SendTokenSymbol component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.SendTokenSymbol.header',
    defaultMessage: 'This is the SendProgress component !',
  },
  token: {
    id: 'app.components.SendTokenSymbol.token',
    defaultMessage: ' Токен: ',
  },
});
