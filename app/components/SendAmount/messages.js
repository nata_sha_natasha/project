/*
 * SendAmount Messages
 *
 * This contains all the text for the SendAmount component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.SendAmount.header',
    defaultMessage: 'Amount to send: ',
  },
  amount: {
    id: 'app.components.SendAmount.amount',
    defaultMessage: 'Количество: ',
  },
});
