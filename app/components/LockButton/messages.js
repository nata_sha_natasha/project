/*
 * LockButton Messages
 *
 * This contains all the text for the LockButton component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.LockButton.header',
    defaultMessage: 'This is the LockButton component !',
  },
  lock: {
    id: 'app.components.LockButton.lock',
    defaultMessage: 'Заблокировать кошелек',
  },
  unlock: {
    id: 'app.components.LockButton.unlock',
    defaultMessage: 'Разблокировать кошелек',
  },
  confirm_locking_wallet: {
    id: 'app.components.LockButton.confirm_locking_wallet',
    defaultMessage: 'Подтвердить блокировку клошелька',
  },
  confirm: {
    id: 'app.components.LockButton.confirm',
    defaultMessage: 'Подтвердить',
  },
  abort: {
    id: 'app.components.LockButton.abort',
    defaultMessage: 'Отменить',
  },
});
