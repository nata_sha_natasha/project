/**
*
* LockButton
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Button, Popconfirm } from 'antd';
// import styled from 'styled-components';

// import { FormattedMessage } from 'react-intl';
import messages from './messages';

function LockButton(props) {
  const { onLockWallet, password, onUnlockWallet } = props;

  if (password) {
    return (
      <Popconfirm key="close_wallet" placement="bottom" title={messages.confirm_locking_wallet.defaultMessage} onConfirm={onLockWallet} okText={messages.confirm.defaultMessage} cancelText={messages.abort.defaultMessage}>
        <Button icon="lock" type="default" size="large" >
          {messages.lock.defaultMessage}
        </Button>
      </Popconfirm>
    );
  }

  return (
    <Button icon="unlock" type="default" size="large" onClick={onUnlockWallet}>
      {messages.unlock.defaultMessage}
    </Button>
  );
}

LockButton.propTypes = {
  onLockWallet: PropTypes.func,
  password: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  onUnlockWallet: PropTypes.func,
};

export default LockButton;
