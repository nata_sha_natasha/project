/*
 * WelcomeText Messages
 *
 * This contains all the text for the WelcomeText component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.WelcomeText.header',
    defaultMessage: 'This is the WelcomeText component !',
  },
  h1_1: {
    id: 'app.components.WelcomeText.h1_1',
    defaultMessage: 'Добро пожаловать в ETH Hot Wallet',
  },
  h1_2: {
    id: 'app.components.WelcomeText.h1_2',
    defaultMessage: 'Для начала создайте или восстановите кошелек Эфириума',
  },
  h2_1: {
    id: 'app.components.WelcomeText.h2',
    defaultMessage: 'ETH Hot Wallet - это нулевой клиент. Подключение к сети Ethereum осуществляется через инфра-локальный узел.',
  },
  h2_2: {
    id: 'app.components.WelcomeText.h2',
    defaultMessage: 'Ключ шифруется с использованием пароля. Когда кошелек заблокирован, вы можете просматривать баланс.',
  },
  h2_3: {
    id: 'app.components.WelcomeText.h2',
    defaultMessage: 'Все ключи сохраняются внутри браузера и никогда не отправляются.',
  },
});
