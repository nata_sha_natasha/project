/**
*
* WelcomeText
*
*/

import React from 'react';
import styled from 'styled-components';

import messages from './messages';

const H1 = styled.h1`
  font-size: 22px;
  color: rgba(0, 0, 0, 0.55);
  font-weight: 400;
`;

const H2 = styled.h2`
font-size: 16px;
margin-top:30px;
color: #b9b9b9;
font-weight: 400;
`;

function WelcomeText() {
  return (
    <div>
      <H1>{messages.h1_1.defaultMessage} <br />{messages.h1_2.defaultMessage}<br /></H1>
      <H2>
        {messages.h2_1.defaultMessage} <br />
        {messages.h2_2.defaultMessage} <br />
        {messages.h2_3.defaultMessage}
      </H2>
    </div>
  );
}

WelcomeText.propTypes = {

};

export default WelcomeText;
