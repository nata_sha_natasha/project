/**
*
* SeedView
*
*/

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
// import { generateKeystore } from 'containers/HomePage/actions';

import messages from './messages';


function SeedView({ loading, error, seed, password, onGenerateKeystore }) {
  if (loading) {
    return <div> {messages.loading.defaultMessage}</div>;
  }

  if (error !== false) {
    return <div> {messages.error.defaultMessage} {error} </div>;
  }

  if (seed !== false) {
    return (
      <div>
        <br />
        {messages.seed.defaultMessage}
        <br />
        {seed}
        <br /><br />
        {messages.keystore_password.defaultMessage}
        <br />
        {password}
        <br />
        <button onClick={onGenerateKeystore} >
          {messages.confirm_seed.defaultMessage}
        </button>
      </div>
    );
  }

  return null;
}

SeedView.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.string,
    PropTypes.bool,
  ]),
  seed: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  password: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  onGenerateKeystore: PropTypes.func,
};


export default SeedView;
