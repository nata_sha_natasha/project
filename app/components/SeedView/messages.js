/*
 * SeedView Messages
 *
 * This contains all the text for the SeedView component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.SeedView.header',
    defaultMessage: 'This is the SeedView component !',
  },
  loading: {
    id: 'app.components.SeedView.loading',
    defaultMessage: 'Загрузка...',
  },
  error: {
    id: 'app.components.SeedView.error',
    defaultMessage: 'Ошибка:',
  },
  seed: {
    id: 'app.components.SeedView.seed',
    defaultMessage: 'Источник:',
  },
  keystore_password: {
    id: 'app.components.SeedView.keystore_password',
    defaultMessage: 'пароль к хранилищу ключей:',
  },
  confirm_seed: {
    id: 'app.components.SeedView.confirm_seed',
    defaultMessage: 'Подтвердить источник',
  },
});
