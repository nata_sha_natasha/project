/*
 * SendTo Messages
 *
 * This contains all the text for the SendTo component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.SendTo.header',
    defaultMessage: 'This is the SendProgress component !',
  },
  send_to_address: {
    id: 'app.components.SendTo.send_to_address',
    defaultMessage: 'Отправить по адресу',
  },
});
