/*
 * NetworkLabel Messages
 *
 * This contains all the text for the NetworkLabel component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.NetworkLabel.header',
    defaultMessage: 'header',
  },
  network_name: {
    id: 'app.components.NetworkLabel.network_name',
    defaultMessage: 'Название сети',
  },
  blockNumber: {
    id: 'app.components.NetworkLabel.blockNumber',
    defaultMessage: 'Номер блока',
  },
  loading_network: {
    id: 'app.components.NetworkLabel.loading_network',
    defaultMessage: 'Загрузка сети',
  },
});
