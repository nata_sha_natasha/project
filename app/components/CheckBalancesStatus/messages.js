/*
 * CheckBalanceStatus Messages
 *
 * This contains all the text for the CheckBalanceStatus component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.CheckBalancesStatus.header',
    defaultMessage: 'check balance status',
  },
  balances_checked_on: {
    id: 'app.components.CheckBalancesStatus.balances_checked_on',
    defaultMessage: 'баланс проверен на',
  },
  balances_wasnt_checked_yet: {
    id: 'app.components.CheckBalancesStatus.balances_wasnt_checked_yet',
    defaultMessage: 'Баланс еще не проверен',
  },
});
