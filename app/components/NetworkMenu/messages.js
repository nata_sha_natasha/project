/*
 * NetworkMenu Messages
 *
 * This contains all the text for the NetworkMenu component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.NetworkMenu.header',
    defaultMessage: 'This is the NetworkMenu component !',
  },
  select_ETH_network: {
    id: 'app.components.NetworkMenu.select_ETH_network',
    defaultMessage: 'Выберите сеть ETH',
  },
});
