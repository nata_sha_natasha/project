/*
 * SendTransactionView Messages
 *
 * This contains all the text for the SendTransactionView component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.SendTransactionView.header',
    defaultMessage: 'This is the SendTransactionView component !',
  },
  sending_transaction: {
    id: 'app.components.SendTransactionView.sending_transaction',
    defaultMessage: 'отправка транзакции...',
  },
  error: {
    id: 'app.components.SendTransactionView.error',
    defaultMessage: 'Ошибка:',
  },
  transaction_send_sucessfully: {
    id: 'app.components.SendTransactionView.transaction_send_sucessfully',
    defaultMessage: 'Транзакция отправлена успешно',
  },
});
