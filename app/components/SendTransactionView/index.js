/**
*
* SendTransactionView
*
*/

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

import messages from './messages';


function SendTransactionView({ sendInProgress, sendError, sendTx }) {
  if (sendInProgress) {
    return <div> {messages.sending_transaction.defaultMessage}</div>;
  }

  if (sendError !== false) {
    return <div> {messages.error.defaultMessage} {sendError} </div>;
  }

  if (sendTx !== false) {
    return (
      <div>
        {messages.transaction_send_sucessfully.defaultMessage} <br />
        TX: {sendTx}
      </div>
    );
  }

  return null;
}

SendTransactionView.propTypes = {
  sendInProgress: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  sendError: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  sendTx: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
};

export default SendTransactionView;
