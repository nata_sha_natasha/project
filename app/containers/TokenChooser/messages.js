/*
 * TokenChooser Messages
 *
 * This contains all the text for the TokenChooser component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.TokenChooser.header',
    defaultMessage: 'This is TokenChooser container !',
  },
  update: {
    id: 'app.containers.TokenChooser.update',
    defaultMessage: 'Обновить',
  },
  remove_tokens: {
    id: 'app.containers.TokenChooser.remove_tokens',
    defaultMessage: 'Удалить токены',
  },
  select_tokens: {
    id: 'app.containers.TokenChooser.select_tokens',
    defaultMessage: 'Выберите токены',
  },
});
