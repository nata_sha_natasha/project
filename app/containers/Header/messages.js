/*
 * Header Messages
 *
 * This contains all the text for the Header component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.Header.header',
    defaultMessage: 'ETH Hot Wallet',
  },
  connected_sucessfully_current_block: {
    id: 'app.containers.Header.connected_sucessfully_current_block',
    defaultMessage: 'Подключен успешно, текущий блок:',
  },
  invalid_JSON_RPC: {
    id: 'app.containers.Header.invalid_JSON_RPC',
    defaultMessage: 'Недействительный ответ JSON RPC от хост-провайдера',
  },
  check_internet: {
    id: 'app.containers.Header.check_internet',
    defaultMessage: 'Проверьте подключение к Интернету и подключение к RPC',
  },
  exchange_rates: {
    id: 'app.containers.Header.exchange_rates',
    defaultMessage: 'Курсы валют успешно обновлены',
  },
  no_man: {
    id: 'app.containers.Header.no_man',
    defaultMessage: 'Нет',
  },
  sure: {
    id: 'app.containers.Header.sure',
    defaultMessage: 'Конечно',
  },
  ropsten_testnet: {
    id: 'app.containers.Header.ropsten_testnet',
    defaultMessage: 'Ropsten Testnet faucet',
  },
  need_some_coins: {
    id: 'app.containers.Header.need_some_coins',
    defaultMessage: 'Нужны монеты для тестирования?',
  },
  sending_request: {
    id: 'app.containers.Header.sending_request',
    defaultMessage: 'Отправка запроса',
  },
  please_wait: {
    id: 'app.containers.Header.please_wait',
    defaultMessage: 'Пожалуйста, подождите',
  },
  faucet_request_sucessfull: {
    id: 'app.containers.Header.faucet_request_sucessfull',
    defaultMessage: 'Запрос к сборке выполнен успешно',
  },
  faucet_request_failed: {
    id: 'app.containers.Header.please_wait',
    defaultMessage: 'Не удалось выполнить запрос к сборке',
  },
  please_try_again_later: {
    id: 'app.containers.Header.please_try_again_later',
    defaultMessage: 'Пожалуйста, повторите попытку позже',
  },
  got_it: {
    id: 'app.containers.Header.got_it',
    defaultMessage: 'Понял',
  },
  check_balance: {
    id: 'app.containers.Header.check_balance',
    defaultMessage: 'Проверьте баланс в течение ~30 секунд. TX:',
  },
  debug_mode: {
    id: 'app.containers.Header.debug_mode',
    defaultMessage: 'Режим отладки:',
  },
  network_not_found: {
    id: 'app.containers.Header.network_not_found',
    defaultMessage: 'сеть не найдена',
  },
  keystore_not_initiated: {
    id: 'app.containers.Header.keystore_not_initiated',
    defaultMessage: 'хранилище ключей не инициировано - Создайте кошелек перед подключением',
  },
  source_address_invalid: {
    id: 'app.containers.Header.source_address_invalid',
    defaultMessage: 'Исходный адрес недействителен',
  },
  amount_must_be_possitive: {
    id: 'app.containers.Header.amount_must_be_possitive',
    defaultMessage: 'Сумма должна быть положительной',
  },
  destenation_address_invalid: {
    id: 'app.containers.Header.destenation_address_invalid',
    defaultMessage: 'Недопустимый адрес места назначения',
  },
  gas_price: {
    id: 'app.containers.Header.gas_price',
    defaultMessage: 'Цена на газ должна быть не менее 0.1 Gwei',
  },
  transaction_created_successfully: {
    id: 'app.containers.Header.transaction_created_successfully',
    defaultMessage: 'Транзакция создана успешно.',
  },
  sending: {
    id: 'app.containers.Header.sending',
    defaultMessage: 'Отправка',
  },
  from: {
    id: 'app.containers.Header.from',
    defaultMessage: 'из',
  },
  to: {
    id: 'app.containers.Header.to',
    defaultMessage: 'в',
  },
  no_password_found: {
    id: 'app.containers.Header.no_password_found',
    defaultMessage: 'Пароль не найден - пожалуйста, разблокируйте кошелек перед отправкой',
  },
  no_keystore_found: {
    id: 'app.containers.Header.no_keystore_found',
    defaultMessage: 'Хранилища ключей не найдено - пожалуйста, создайте кошелек',
  },
  not_found: {
    id: 'app.containers.Header.not_found',
    defaultMessage: 'не найден',
  },
  contract_address_for_token: {
    id: 'app.containers.Header.contract_address_for_token',
    defaultMessage: 'Контрактный адрес для токена',
  },
  faucet_not_ready: {
    id: 'app.containers.Header.faucet_not_ready',
    defaultMessage: 'сборщик не готов',
  },
});
