/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.HomePage.header',
    defaultMessage: 'ETH Hot Wallet',
  },
  wallet_locked_succesfuly: {
    id: 'app.components.HomePage.wallet_locked_succesfuly',
    defaultMessage: 'Кошелек заблокирован успешно',
  },
  wallet_unlocked_succesfuly: {
    id: 'app.components.HomePage.wallet_unlocked_succesfuly',
    defaultMessage: 'Кошелек разблокирован успешно',
  },
  wallet_removed_from_memory: {
    id: 'app.components.HomePage.wallet_removed_from_memory',
    defaultMessage: 'Кошелек удален из памяти',
  },
  invalid_seed: {
    id: 'app.components.HomePage.invalid_seed',
    defaultMessage: 'Недопустимое количество источников',
  },
  password_length: {
    id: 'app.components.HomePage.password_length',
    defaultMessage: 'Длина пароля должна быть не менее 8 символов.',
  },
  please_enter_keystore_password: {
    id: 'app.components.HomePage.please_enter_keystore_password',
    defaultMessage: 'Введите пароль хранилища ключей',
  },
  password: {
    id: 'app.components.HomePage.password',
    defaultMessage: 'Пароль',
  },
  genKeystore_error: {
    id: 'app.components.HomePage.genKeystore_error',
    defaultMessage: 'Ошибка генерации хранилища ключей',
  },
  no_keystore_found: {
    id: 'app.components.HomePage.no_keystore_found',
    defaultMessage: 'Хранилище ключей не найдено',
  },
  wallet_locked: {
    id: 'app.components.HomePage.wallet_locked',
    defaultMessage: 'Кошелек заблокирован',
  },
  wallet_already_unlocked: {
    id: 'app.components.HomePage.wallet_already_unlocked',
    defaultMessage: 'Кошелек уже разблокирован',
  },
  no_keystore_to_unlock: {
    id: 'app.components.HomePage.no_keystore_to_unlock',
    defaultMessage: 'Нет хранилища ключей для разблокировки',
  },
  no_password_entered: {
    id: 'app.components.HomePage.no_password_entered',
    defaultMessage: 'Пароль не введен',
  },
  invalid_password: {
    id: 'app.components.HomePage.invalid_password',
    defaultMessage: 'Неверный пароль',
  },
  unlock_wallet_error: {
    id: 'app.components.HomePage.unlock_wallet_error',
    defaultMessage: 'Ошибка разблокировки кошелька',
  },
  no_keystore_defined: {
    id: 'app.components.HomePage.no_keystore_defined',
    defaultMessage: 'Хранилище ключей не определено',
  },
  existing_keystore: {
    id: 'app.components.HomePage.existing_keystore',
    defaultMessage: 'Существующее хранилище ключей - прерывание загрузки формы локального хранения',
  },
  no_keystore_found_in: {
    id: 'app.components.HomePage.header',
    defaultMessage: 'Хранилище ключей не найдено в локальном хранилище',
  },
});
