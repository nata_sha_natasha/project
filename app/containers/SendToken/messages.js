/*
 * SendToken Messages
 *
 * This contains all the text for the SendToken component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.SendToken.header',
    defaultMessage: 'This is SendToken container !',
  },
  transaction_confirmed: {
    id: 'app.containers.SendToken.transaction_confirmed',
    defaultMessage: 'Транзакция подтверждена успешно, Отправить для передачи',
  },
  create_transaction: {
    id: 'app.containers.SendToken.create_transaction',
    defaultMessage: 'Создать транзакцию',
  },
  send_token: {
    id: 'app.containers.SendToken.send_token',
    defaultMessage: 'Отправить токен',
  },
});
